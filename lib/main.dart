import 'package:flutter/material.dart';
enum APP_THEME{LIGHT,DARK}
void main() {
  runApp(ContactProfilePage());
}
class MyappTheme{
  static ThemeData appThemeLight(){
    return ThemeData(
      brightness: Brightness.light,
      appBarTheme: AppBarTheme(
        color: Colors.tealAccent,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.teal,
      ),
    );
  }
  static ThemeData appThemeDark(){
    return ThemeData(
      brightness: Brightness.dark,
      appBarTheme: AppBarTheme(
        color: Colors.tealAccent,
        iconTheme: IconThemeData(
          color: Colors.black87,
        ),
      ),
      iconTheme: IconThemeData(
        color: Colors.orange,
      ),
    );
  }
}
class ContactProfilePage extends StatefulWidget {
  @override
  State<ContactProfilePage> createState() => _ContactProfilePageState();
}

class _ContactProfilePageState extends State<ContactProfilePage> {
  var currentTheme = APP_THEME.LIGHT;

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: currentTheme == APP_THEME.DARK
          ? MyappTheme.appThemeLight()
          : MyappTheme.appThemeDark() ,
      home: Scaffold(
        appBar: buildAppbarWidget(),
        body: buliBodyWidget(),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.change_circle),
            onPressed: (){
              setState(() {
                currentTheme == APP_THEME.DARK
                    ?currentTheme = APP_THEME.LIGHT
                    :currentTheme = APP_THEME.DARK;
              });
            },
        ),
      ),
    );
  }
}

Widget buildCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.call,
         // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Call"),
    ],
  );
}

Widget buildTextButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.message,
          //color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("text"),
    ],
  );
}

Widget buildVideoCallButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.video_call,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Video"),
    ],
  );
}

Widget buildEmailButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.email,
          //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Email"),
    ],
  );
}

Widget buildDirectionsButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.directions,
          // color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Direction"),
    ],
  );
}

Widget buildPayButton() {
  return Column(
    children: <Widget>[
      IconButton(
        icon: Icon(
          Icons.attach_money,
          //  color: Colors.indigo.shade800,
        ),
        onPressed: () {},
      ),
      Text("Pay"),
    ],
  );
}

Widget mobilePhoneListTile() {
  return ListTile(
    leading: Icon(Icons.call),
    title: Text("330-802-3365"),
    subtitle: Text("mobile"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget otherPhoneListTile() {
  return ListTile(
    leading: Text(''),
    title: Text("440-440-3365"),
    subtitle: Text("other"),
    trailing: IconButton(
      icon: Icon(Icons.message),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget emailListTile() {
  return ListTile(
    leading: Icon(Icons.email),
    title: Text("DomeMetal@hotmail.com"),
    subtitle: Text("work"),
  );
}

Widget addressListTile() {
  return ListTile(
    leading: Icon(Icons.location_pin),
    title: Text("012 Sunrise takemehome to the place"),
    subtitle: Text("home"),
    trailing: IconButton(
      icon: Icon(Icons.directions),
      color: Colors.teal,
      onPressed: () {},
    ),
  );
}

Widget buliBodyWidget() {
  return ListView(
    children: <Widget>[
      Column(
        children: <Widget>[
          Container(
            width: double.infinity,

            //Height constraint at Container widget level
            height: 250,

            child: Image.network(
              "https://scontent.fbkk21-1.fna.fbcdn.net/v/t1.6435-9/49067579_1201870303325217_1509565508578443264_n.jpg?_nc_cat=104&ccb=1-7&_nc_sid=174925&_nc_eui2=AeG901XRhoCk_qRQm2Nmv7nE527c-eXDC1jnbtz55cMLWL5ewuxF-OP2Bm7oHwIz8RXb_mAikpbbqIska0cBvzUY&_nc_ohc=CbaUJZl3B-cAX86C0Nl&_nc_ht=scontent.fbkk21-1.fna&oh=00_AfClo5WwrAu82a_pVgwN95ea-n7LP28OohxmxHd-T8JZXQ&oe=63DD0D3D",
              fit: BoxFit.cover,
            ),
          ),
          Container(
            height: 60,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(8.5),
                  child: Text(
                    "Tewly Kuy",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontSize: 30),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          Container(
            margin: EdgeInsets.only(top: 8, bottom: 8),
            child: Theme(
              data: ThemeData(iconTheme: IconThemeData(color: Colors.pink)),
              child: buildButtonWidget(),
            ),
          ),
          Divider(
            color: Colors.black,
          ),
          mobilePhoneListTile(),
          otherPhoneListTile(),
          Divider(
            color: Colors.black,
          ),
          emailListTile(),
          Divider(
            color: Colors.black,
          ),
          addressListTile(),
        ],
      ),
    ],
  );
}

AppBar buildAppbarWidget() {
  return AppBar(
    backgroundColor: Colors.tealAccent,
    leading: Icon(
      Icons.arrow_back,
      color: Colors.red,
    ),
    actions: <Widget>[
      IconButton(
        onPressed: () {},
        icon: Icon(Icons.star_border),
        color: Colors.red,
      )
    ],
  );
}

Widget buildButtonWidget() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    children: <Widget>[
      buildCallButton(),
      buildTextButton(),
      buildVideoCallButton(),
      buildEmailButton(),
      buildDirectionsButton(),
      buildPayButton(),
    ],
  );
}
